<?php

use CodingPaws\PSpec\Config\Config;
use CodingPaws\PSpec\Console\DocTestFormatter;
use CodingPaws\PSpec\Console\DotTestFormatter;

require './spec/mocks/Basket.php';
require './spec/mocks/BasketEntity.php';
require './spec/mocks/User.php';
require './spec/mocks/UserEntity.php';

return Config::new()
  ->setFormatter(getenv('CI') ? new DocTestFormatter : new DotTestFormatter)
  ->logJUnit('junit.xml');
