# Expose

Expose is a framework to transform your classes (e.g. Laravel entities)
to API entities.

## Example

Install: `composer require codingpaws/expose`

```php
use CodingPaws\Expose\HasEntity;
use CodingPaws\Expose\Entity;

// Dog.php
class Dog implements HasEntity {
  public function __construct(public string $name, public string $breed)
  {
  }
}

// DogEntity.class
class DogEntity extends Entity {
  public function __invoke(): void
  {
    $this->expose('name');
    $this->expose('breed');
    $this->expose('cute', true);
  }
}

// In your controller
$result = Entity::transform(new Dog('Lacey', 'Border Collie'));

// Generates an array like
$result = [
  'name' => 'Lacey',
  'breed' => 'Border Collie',
  'cute' => true,
];
```
