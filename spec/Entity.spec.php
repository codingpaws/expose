<?php

use CodingPaws\Expose\Entity;
use Spec\Mocks\User;

describe(Entity::class, function () {
  subject(fn () => Entity::transform(get('entity'), get('options')));
  let('options', []);
  let('base_result', [
    'name' => 'doggo',
    'full_name' => 'Doggo Dog',
    'followers' => 2,
    'is_admin' => false,
    'random_number' => 3,
  ]);

  context('with a single entity', function () {
    let('entity', new User);

    it('is transformed', function () {
      expect(subject())->toBe($this->base_result);
    });
  });

  context('with a list of entities', function () {
    let('entity', [new User, new User, new User]);

    it('is transformed', function () {
      expect(subject())->toBe([
        $this->base_result,
        $this->base_result,
        $this->base_result,
      ]);
    });
  });

  context('with options', function () {
    let('entity', new User);
    let('options', [
      User::class => ['admin'],
    ]);

    it('has more fields than the base result', function () {
      expect(ksort_inplace(subject()))->toBe(ksort_inplace(array_merge([
        'email' => 'doggo@woof.com',
      ], $this->base_result)));
    });
  });

  context('with nested transformable objects', function () {
    let('entity', collect([new User]));
    let('options', [
      User::class => [
        'basket' => 'show',
      ],
    ]);

    it('is transformed recursively', function () {
      expect(subject()[0]['basket'])->toBe([
        'id' => 42,
        'user_id' => 1,
      ]);
    });
  });

  describe('with a string that is technically callable', function () {
    let('function', 'exec');
    let('entity', fn () => new User(get('function')));

    it('transforms the entity without calling the string', function () {
      expect(is_callable($this->function))->toBe(true);
      expect(subject()['name'])->toBe($this->function);
    });
  });
});

function ksort_inplace(array $array): array
{
  ksort($array);

  return $array;
}
