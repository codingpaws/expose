<?php

namespace Spec\Mocks;

use CodingPaws\Expose\Entity;
use CodingPaws\Expose\HasEntity;

class Basket implements HasEntity
{
  public function __construct(
    public int $user_id = 1,
    public int $id = 42,
  ) {
  }

  public function toEntity(): Entity
  {
    return new BasketEntity($this);
  }
}
