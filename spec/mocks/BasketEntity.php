<?php

namespace Spec\Mocks;

use CodingPaws\Expose\Entity;

class BasketEntity extends Entity
{
  public function __invoke(): void
  {
    $this->expose('id');
    $this->expose('user_id');
  }
}
