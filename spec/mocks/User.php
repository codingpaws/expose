<?php

namespace Spec\Mocks;

use CodingPaws\Expose\Entity;
use CodingPaws\Expose\HasEntity;

class User implements HasEntity
{
  public function __construct(
    public string $name = 'doggo',
    public string $full_name = 'Doggo Dog',
  ) {
  }

  public function toEntity(): Entity
  {
    return new UserEntity($this);
  }

  public function __get(string $key)
  {
    return match ($key) {
      'id' => 1,
      'email' => 'doggo@woof.com',
      'followers' => 2,
      'basket' => new Basket,
      default => null,
    };
  }

  public function isAdmin(): bool
  {
    return false;
  }
}
