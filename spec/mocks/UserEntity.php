<?php

namespace Spec\Mocks;

use CodingPaws\Expose\Entity;

class UserEntity extends Entity
{
  public function __construct(private User $user)
  {
    parent::__construct($user);
  }

  public function __invoke(): void
  {
    $this->expose('name', $this->user->name);
    $this->expose('full_name');
    $this->expose('email', if_option: 'admin');
    $this->expose('basket', if: $this->getOption('basket') === 'show');
    $this->expose('followers');
    $this->expose('is_admin', $this->user->isAdmin());
    $this->expose('random_number', method: true);
  }

  public function randomNumber(): int
  {
    return 3;
  }
}
