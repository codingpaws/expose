<?php

namespace CodingPaws\Expose;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

abstract class Entity implements Arrayable
{
  private array $exposes = [];
  private array $options = [];
  private array $allOptions = [];

  public function __construct(private object $object)
  {
  }

  abstract public function __invoke(): void;

  /**
   * @param string $name
   * @param mixed $value = null
   * @param bool $if = true
   * @param bool $method = false
   */
  public function expose(...$args)
  {
    if ($this->isSkipped($args)) {
      return;
    }

    $name = $args[0];

    if (array_key_exists(1, $args)) {
      $value = $args[1];
    } else if ($args['method'] ?? false) {
      $value = $this->{Str::camel($name)}();
    } else {
      $value = $this->object->{$name};
    }

    $value = !is_string($value) && is_callable($value) ? $value() : $value;

    $this->exposes[$name] = $value;
  }

  private function isSkipped(array $args): mixed
  {
    $option = $args['if_option'] ?? null;

    if ($option) {
      return !$this->getOption($option);
    }

    $if = !array_key_exists('if', $args) || $args['if'] ?? true;

    return !$if;
  }

  private function withOptions(array $options): self
  {
    $this->allOptions = $options;
    $this->options = $options[$this->object::class] ?? [];

    return $this;
  }

  protected function hasOption(string $name): bool
  {
    return array_key_exists($name, $this->options) || in_array($name, $this->options);
  }

  protected function getOption(string $name): mixed
  {
    return $this->options[$name] ?? in_array($name, $this->options);
  }

  public function toArray()
  {
    $this->exposes = [];
    $this->__invoke();

    return self::transform($this->exposes, $this->allOptions);
  }

  public static function transform(mixed $value, array $options = []): mixed
  {
    if ($value instanceof Collection) {
      return self::transform($value->all(), $options);
    }

    if (is_iterable($value)) {
      foreach ($value as $k => $v) {
        $value[$k] = self::transform($v, $options);
      }
    } else if ($value instanceof Entity) {
      return $value->withOptions($options)->toArray();
    } else if ($value instanceof HasEntity) {
      return self::transform($value->toEntity(), $options);
    } else if ($value instanceof Arrayable) {
      return self::transform($value->toArray(), $options);
    }

    return $value;
  }
}
