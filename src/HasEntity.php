<?php

namespace CodingPaws\Expose;

interface HasEntity
{
  public function toEntity(): Entity;
}
