<?php

namespace CodingPaws\Expose\Middleware;

use Closure;
use CodingPaws\Expose\Entity;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class TransformResponseEntites
{
  public function handle($request, Closure $next)
  {
    $response = $next($request);

    if ($this->canTransform($response)) {
      $data = Entity::transform($response->original);

      if ($response instanceof JsonResponse) {
        $response->setData($data);
      } else {
        $response->setContent(json_encode($data));
      }
    }

    return $response;
  }

  private function canTransform(mixed $response): bool
  {
    return $response instanceof JsonResponse || ($response instanceof Response && $response->headers->get('content-type') === 'application/json');
  }
}
